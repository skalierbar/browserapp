<?php

namespace Helper;

class BottomLoader extends \Prefab
{

  static public function render($node)
  {
    $f3 = \Base::instance();

    $bottomscripts = $f3->get('bottomscripts');

    return "<script>".$bottomscripts."</script>";
  }

}
