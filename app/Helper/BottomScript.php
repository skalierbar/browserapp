<?php

namespace Helper;

class BottomScript extends \Prefab
{

  static public function render($node)
  {
    $f3 = \Base::instance();

    $bottomscripts = $f3->get('bottomscripts');

    $bottomscripts = $bottomscripts." ".$node[0];

    $f3->set('bottomscripts', $bottomscripts);

    return NULL;
  }

}
