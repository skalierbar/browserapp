<?php

namespace Helper;

use League\CommonMark\DocParser;
use League\CommonMark\Environment;
use League\CommonMark\HtmlRenderer;

use League\CommonMark\CommonMarkConverter;

class MarkdownHelper extends \Prefab
{
    public function convert($markdown)
    {
      $environment = Environment::createCommonMarkEnvironment();

      $parser = new DocParser($environment);
      $htmlRenderer = new HtmlRenderer($environment);

      $document = $parser->parse($markdown);
      return $htmlRenderer->renderBlock($document);
    }

}
