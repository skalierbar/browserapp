<?php

namespace Controller;

class Sitemaps extends AppController
{

	public function afterRoute()
	{
		//
	}

	public function view($f3, $args)
	{
		$Sitemap = new \Builder\Sitemap();
		$sitemap_urls = $Sitemap->getSitemapUrls($f3['SERVER']['HTTP_HOST'],$f3['SCHEME']);

		$sitemap = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

		foreach ($sitemap_urls as $sitemap_url)
		{
    	$sitemap .= PHP_EOL.'<url><loc>'.$sitemap_url.'</loc></url>';
    }

		$sitemap .=PHP_EOL.'</urlset>';

		header('Content-Type: text/xml; charset=UTF-8');
		echo $sitemap;
	}
}
