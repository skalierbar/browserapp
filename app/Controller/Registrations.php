<?php

namespace Controller;

class Registrations extends AppController
{

  public function store($f3, $args)
  {
    $function = $f3->get('ROUTES.'.$args[0].'.0.GET.0');

		if(class_exists('\Extension\Registrations')) {
      $ExtensionRegistrations = new \Extension\Registrations;
      $response = $ExtensionRegistrations->store($f3);
      $f3->set('response', $response);
    }

    if(!empty($function)) {
      $identifiers = explode('->', $function);
      $obj = new $identifiers[0];
      call_user_func_array([$obj,'view'],[$f3,$args]);
    }
    else {
      (new \Controller\Pages)->view($f3, $args);
    }

  }

}
