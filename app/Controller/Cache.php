<?php

namespace Controller;

class Cache extends AppController
{

	public function destroy()
	{
		$this->cache->reset();
		$this->deleteRoutesFile();

		\Base::instance()->reroute('/');
		// $this->cleanCfCache();
		// $this->warmUpCfCache();
	}

	private function deleteRoutesFile()
	{
		$RouteBuilder = new \Builder\Routes;
		$RouteBuilder->deleteRoutesFile();
	}


	// public function warmUpCfCache()
	// {
	// 	$cf = $this->getCf();
	// 	$f3 = \Base::instance();
	// 	$warmer = new \Contentful\Delivery\Cache\CacheWarmer($cf);
	// 	$warmer->warmUp('../tmp');
	// }
  //
  //
	// public function cleanCfCache()
	// {
	// 	$f3 = \Base::instance();
	// 	$clearer = new \Contentful\Delivery\Cache\CacheClearer($f3->get('cf_space_id'));
	// 	$clearer->clear('../tmp');
	// }
  //
  //
	// private function getCf()
	// {
	// 	$f3 = \Base::instance();
  //
	// 	$space = $f3->get('cf_space_id');
	// 	$token = $f3->get('cf_access_key');
	// 	$preview = false;
	// 	$locale = $f3->get('locale');
  //
	// 	$cf = new \Contentful\Delivery\Client($token, $space, $preview, $locale, ['cacheDir' => $f3->get('TEMP')]);
	// 	return $cf;
	// }

}
