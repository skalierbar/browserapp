<?php

namespace Controller;

class Pages extends AppController
{

	private function execContentTypeMiddleware($contentType, $page, Pages $instance = NULL)
	{
		if(class_exists('\Middleware\\'.ucfirst($contentType)))
		{
			$f3 = \Base::instance();
			$class = "\Middleware\\".ucfirst($contentType);
			$Middleware = new $class;
			$Middleware->init($f3, $page, $instance);
		}
	}

	public function view($f3, $args)
	{
		$posts = NULL;

		$Entry = new \Service\Contentful\Entry(['slug' => $args[0]]);
		$result = $Entry->getFields();

		if(!$result) $f3->error(404);

		if(!empty($result['layout'])) $this->layout = $result['layout'];
			else $this->layout = 'default';

		$layout = $this->getLayout($this->layout);

		$view = $result['sys']['contentType'];
		$this->execContentTypeMiddleware($view, $result, $this);

		if(isset($result['sections']))
		{
			foreach($result['sections'] as $section)
			{
				if(class_exists('\Section\\'.$section['sys']['contentType'])) {
					$class = "\Section\\".$section['sys']['contentType'];
					$ExtensionSection = new $class;
					$ExtensionSection->init($f3, $section, $result);
				}
			}
		}

		$f3->set('layout', $layout);
		$f3->set('slug', $slug);
		$f3->set('metaInformation', $result['metaInformation']);
		$f3->set('data', $result);
		$f3->set('sections', $result['sections']);
		$f3->set('body','pages/'.$view.'.html');
	}

}
