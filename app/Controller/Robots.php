<?php

namespace Controller;

class Robots extends AppController
{

	public function afterRoute() {
    //
  }

	public function view($f3, $args)
	{
		$records = [
      [
        'User-agent' => '*',
        'Disallow' => '/cache/clean'
      ],
      [
        'Sitemap' => $f3['SCHEME'].'://'.$f3['SERVER']['HTTP_HOST'].'/sitemap.xml'
      ]
    ];

    $robotsTXT = '';

    foreach ($records as $record)
		{
    	foreach($record as $keyword => $value)
      {
        $robotsTXT .= $keyword.": ".$value. "\n";
      }
      $robotsTXT .= "\n";
    }

		header('Content-Type: text/plain; charset=UTF-8');
		echo $robotsTXT;
	}
}
