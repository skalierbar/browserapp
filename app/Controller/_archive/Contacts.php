<?php

namespace Controller;

use Mailgun\Mailgun;

//! Front-end processor
class Contacts extends AppController
{

  public function store($f3, $args)
  {
    $data = [
      'name' => $f3->get('POST.name'),
      'email' => $f3->get('POST.email'),
      'phone' => $f3->get('POST.phone'),
      'message' => $f3->get('POST.message'),
      'success' => $f3->get('POST.success'),
    ];

    $to = 'info@einer-fuer-alle.berlin';
    $from = 'wms-notification@echteberliner.com';
    $subject = '1 Neue Kontaktanfrage';
    $message = $data['name'].', '.$data['email'].', '.$data['phone'].','.$data['message'];

    $mg = Mailgun::create($f3->get('mg_key'));

    $response = $mg->messages()->send('sandboxdf680bda28f64a5b8263bb8dea997090.mailgun.org', [
      'from'    => $from,
      'to'      => $to,
      'subject' => $subject,
      'text'    => $message
    ]);

    $f3->reroute('/'.$data['success']);
  }

}
