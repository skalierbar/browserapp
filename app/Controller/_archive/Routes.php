<?php

namespace Controller;

class Routes extends AppController
{

	public function view($f3, $args)
	{
		$slug = $args[0];

		if($slug == '/') $slug = $f3->get('HOMESLUG');
		else $slug = substr($slug, 1);

		$posts = NULL;

		$result = (new \Service\Contentful\Base)->getEntryBySlug($slug);

		$view = $result['body']['sys']['contentType'];

		if(!empty($result['layout']['name'])) {
			$this->layout = $result['layout']['name'];

		}
		else {
			$this->layout = 'default';
		}

		$layout = $this->getLayout($this->layout);

		$content = $result['body'];

		$metaInformation = $result['metaInformation'];

		if(class_exists('\Extension\Route')) {
      $Instance = new \Extension\Route;
      $Instance->view($f3, [
				'slug' => $slug,
				'contentType' => $view
			]);
    }

		if(isset($result['visitorInformation']))
		{
			foreach($result['visitorInformation'] as $key => $value)
			{
				if($key != "sys") $f3->set('COOKIE.visitorInformation-'.$key, $result['visitorInformation'][$key], 86400);
			}
		}

		$f3->set('layout', $layout);
		$f3->set('slug', $slug);
		$f3->set('metaInformation', $metaInformation);
		$f3->set('data', $content);
		$f3->set('body','pages/'.$view.'.html');
	}

}
