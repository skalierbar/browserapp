<?php

namespace Controller;

use GuzzleHttp\Client;

abstract class AppController
{

	protected $layout = 'default';
	protected $cache;

	function __construct()
	{
		$this->cache = \Cache::instance();

		$f3 = \Base::instance();
		$UI = $f3->get('UI');

	}

	public function beforeroute()
	{
		if(class_exists('\Extension\Instance')) {
      $Instance = new \Extension\Instance();
      $Instance->beforeRoute();
    }
	}

	public function afterroute()
	{
		$this->setLocation();
		$this->setFullUrl();
		$this->setSDC();

		\Template::instance()->extend('bottomloader','\Helper\BottomLoader::render');
		\Template::instance()->extend('bottomscript','\Helper\BottomScript::render');
		\Template::instance()->filter('convert','\Helper\MarkdownHelper::instance()->convert');

		if(!empty($this->layout) && $this->layout != false)
		{
			echo \Template::instance()->render('layouts/'.$this->layout.'.html');
    }
	}

	private function setSDC()
	{
		$f3 = \Base::instance();
		if(!empty($f3->get('COOKIE.sdc-uid')))
		{
			$dataLayer = [
				'userId' => $f3->get('COOKIE.sdc-uid')
			];

			$f3->set('dataLayer', $dataLayer);
		}
	}

	private function setLocation()
	{
		$location = \Base::instance()->get('COOKIE.location');

		if(!empty($location))
		{
			$geo = \Web\Geo::instance();

			if(empty($location = $geo->location()))
			{
				$location = 'unknown';
			}

		}

		\Base::instance()->set('location', $location);
	}

	protected function getContentTypeByController()
	{
		$class = explode('\\', get_class($this));

		$routeTypeMapping = \Base::instance()->get('RouteTypeMapping');

		return $routeTypeMapping[$class[1]];
	}

	protected function getLayout($title)
	{
		$cf = new \Service\Contentful\Base;
		$layout = $cf->getLayoutByTitle($title);
		return $layout;
	}

	protected function setFullUrl()
	{
		$uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);

		if( isset($_SERVER['HTTPS'] ) ) $protocol = 'https://'; else $protocol = 'http://';

		\Base::instance()->set('fullUrl', $protocol . $_SERVER['HTTP_HOST'] . $uri_parts[0]);
	}

	public function setLayout($layout) {
		$this->layout = $layout;
	}
}
