<?php

namespace Controller;

class BlogCategories extends AppController
{

	public function view($f3, $args)
	{
		$slug = $args[0];

		if($slug == '/') $slug = $f3->get('HOMESLUG');
		else $slug = substr($slug,1);

		// $Category = new \Model\BlogCategory();
		// $result = $Category->getCategoryBySlug($slug);

		$result = (new \Service\Contentful)->getEntryBySlug($slug);

		$layout = $this->getLayout('default');

		$view = $result['sys']['contentType'];
		$content = $result;
		$metaInformation = $content['metaInformation'];

		$Posts = new \Model\BlogPost();
		$posts = $Posts->getRecentPosts('blogPost', '-fields.publishDate');

		$f3->set('blogPosts', $posts);
		$f3->set('layout', $layout);
		$f3->set('slug', $slug);
		$f3->set('metaInformation', $metaInformation);
		$f3->set('data', $content);
		$f3->set('body','pages/'.$view.'.html');
	}

}
