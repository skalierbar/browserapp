<?php

namespace Builder;

class Instance
{

  public function __construct()
  {
    $f3 = \Base::instance();
    $this->config();
  }

  public function config()
  {
    $f3 = \Base::instance();
    $version = $f3->get('VERSION');

    $cf = new \Service\Contentful\Base;
    $config = $cf->getConfig($version);

    if(!empty($config))
    {
      foreach($config as $param => $value)
      {
        $f3->set(strtoupper($param), $value);
      }
      
      $f3->set('UI', '../ui/'.$f3->get('THEME').'/');
      $f3->set('ASSETS', '/'.$f3->get('THEME').'/');
    }
  }

}
