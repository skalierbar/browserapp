<?php

namespace Builder;

class Sitemap
{

    public function getSitemapUrls($base, $scheme)
    {
      $sitemap_urls = [];

      $routes = \Base::instance()->get('ROUTES');

      foreach($routes as $slug => $data)
      {
        if(!empty($data[0]['GET'][3]))
          $sitemap_urls[] = $scheme.'://'.$base.$slug;
      }

      return $sitemap_urls;
    }

}
