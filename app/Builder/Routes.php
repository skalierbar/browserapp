<?php

namespace Builder;

class Routes
{
  private $cache;
  private $ContentTypeOfSlug;

  public function __construct()
  {
    $this->cache = \Cache::instance();
  }

  public function getRoutesFile($f3, $setup = false)
  {
    $filePath = $f3->get('TEMP').'routes.ini';

    if(empty($f3->read($filePath)) || $setup == true) $this->buildRoutesFile($f3);

    return $filePath;
  }


  private function buildRoutesFile($f3 = '')
  {
    $f3 = \Base::instance();

    $file = $f3->get('TEMP').'routes.ini';

    $data = "[routes]\n\n";

    $routeTypeMapping = $f3->get('RouteTypeMapping');

    if(empty($routeTypeMapping)) $routeTypeMapping = $f3->ROUTECLASSIFICATION;

    $appRoutes= [];

    if(is_array($routeTypeMapping))
    {
      foreach($routeTypeMapping as $controller => $contentType)
      {
        if(is_array($contentType)) {

          foreach($contentType as $item) {

            $routesOfContentType = $this->getRoutesOfContentType($item, $controller);

            if(is_array($routesOfContentType))
            {

              foreach($routesOfContentType as $route)
              {
                if(!empty($route[2])) $this->cacheContentTypeOfSlug($route[2], $item);
              }

              if(!empty($routesOfContentType)) $appRoutes = array_merge($appRoutes, $routesOfContentType);

            }

          }
        }
        else {

          $routesOfContentType = $this->getRoutesOfContentType($contentType, $controller);

          if(is_array($routesOfContentType))
          {

            foreach($routesOfContentType as $route) {
              if(!empty($route[2])) $this->cacheContentTypeOfSlug($route[2], $contentType);
            }

            if(!empty($routesOfContentType)) $appRoutes = array_merge($appRoutes, $routesOfContentType);
          }
        }
      }
    }

    $appRoutes = array_merge($appRoutes, $this->getDefaultRoutes()); // add default routes

    foreach($appRoutes as $route)
    {
      $data .= $route[0]." = ".$route[1]. "\n\n";
    }

    $f3->write($file, $data);

    return $file;
  }

  private function getDefaultRoutes()
  {
    $routes = [
      [
        'GET|POST /cache/clean',
        '\Controller\Cache->destroy'
      ],
      [
        'GET /sitemap.xml',
        '\Controller\Sitemaps->view'
      ],
      [
        'GET /robots.txt',
        '\Controller\Robots->view'
      ],
    ];
    return $routes;
  }

  // private function setIdOfSlug($slug, $id)
  // {
  //   echo $slug = str_replace('/', '-', $slug);
  //   return $this->cache->set($slug.'-id', $id);
  // }

  private function getRoutesOfContentType($contentType, $controller, $callData = ['skip' => 0,'limit' => 1000])
  {
    $connector = new \Service\Contentful\Base;

    $response = $connector->getEntries($contentType, ['fields.slug','sys.id'], $callData);

    $limit = $response->limit;
    $skip = $response->skip;
    $total = $response->total;

    $homeroute = \Base::instance()->get('HOMESLUG');

    foreach($response->items as $item)
    {
      $id = $item->sys->id;
      $slug = $item->fields->slug;

      if(empty($slug)) $slug = $id;

      // $this->setIdOfSlug($slug, $id);

      if($slug == $homeroute) $slug = '';

      $routes[] = [
        'GET @'. $id .': /'.$slug,
        '\\Controller\\'. $controller .'->view',
        $item->fields->slug
      ];

    }

    if($total > ($skip + $limit)) {
      $callData = [
        'skip' => $skip + $limit,
        'limit' => $limit,
      ];

      $routes = array_merge($routes, $this->getRoutesOfContentType($contentType, $controller, $callData));
    }

    return $routes;
  }

  private function cacheContentTypeOfSlug($slug, $contentType)
  {
    $cache = \Cache::instance();

    $slug = str_replace('/','--',$slug);

    $this->ContentTypeOfSlug[$slug] = $contentType;

    return $cache->set($slug, $contentType);
  }


  public function deleteRoutesFile()
  {
    $file = \Base::instance()->get('TEMP').'routes.ini';
    return unlink($file);
  }

  public function getContentTypeOfSlug($slug)
  {
    $contentType = false;

    $slug = str_replace('/','--',$slug);

    if(empty($this->ContentTypeOfSlug[$slug])) {
      $this->buildRoutesFile();
    }
    $contentType = $this->ContentTypeOfSlug[$slug];
    return $contentType;
  }


}
