<?php

namespace Service\Contentful;

class EntryCollection extends Base
{

  public function getItems($contentType, $params = [])
  {
    $entries = [];
    $callId = 'entries';

    $params['query'] = $params;
    $params['query']['content_type'] = $contentType;

    $response = $this->call($callId, $params);

    if($response->total >= 1)
    {
      foreach($response->items as $item)
      {
        $Entry = new Entry(['id' => $item->sys->id]);
        $entries[] = $Entry->getFields();
      }
    }
    return $entries;
  }

}
