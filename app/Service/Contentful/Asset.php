<?php

namespace Service\Contentful;

class Asset extends Base
{
  private $identifier;
  private $fields;
  private $includes;

  public function __construct($id)
  {
    parent::__construct();

    $this->id = $id;
  }

  public function getFields()
  {
    $result = [];

    if($this->isPreview() || $this->cache->exists('asset-'.$this->id, $result) === false )
    {
      $callId = 'assets/'.$this->id;
      $item = $this->call($callId);
      $result = json_decode(json_encode($item->fields), true);
    }
    return $result;
  }


}
