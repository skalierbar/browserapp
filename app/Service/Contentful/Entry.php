<?php

namespace Service\Contentful;

class Entry extends Base
{
  private $id;
  private $sublevel;
  private $includes;

  public function __construct($identifier, $sublevel = 0)
  {
    parent::__construct();

    if(!empty($identifier['id']))
    {
      $this->id = $identifier['id'];
    }
    elseif(!empty($identifier['slug']))
    {
      $slug = $identifier['slug'];
      $this->id = $this->getIdBySlug($slug);
    }

    $this->sublevel = $sublevel;
  }

  public function getFields()
  {
    $result = [];

    if($this->isPreview() || $this->cache->exists($this->id, $result) === false)
    {
      if(!empty($this->id))
      {
        $callId = 'entries';
        $params = [
          'query' => [
            'include' => 4,
            'sys.id' => $this->id
          ]
        ];
        $response = $this->call($callId, $params);

        if($response->total == 1)
        {
          $this->includes = $response->includes;
          $item = $response->items[0];

          if(!empty($item))
          {
            foreach($item->fields as $field => $value)
            {
              if($this->isLink($value)) $value = $this->getChildren($value);

              $result[$field] = json_decode(json_encode($value), true);
            }

            $result['sys'] = json_decode(json_encode($item->sys), true);
            $result['sys']['contentType'] = $result['sys']['contentType']['sys']['id'];
          }
          $this->cache->set($this->id, $result);
        }
      }

    }

    return $result;
  }

  private function isLink($item)
  {
    $result = false;

    if (isset($item->sys) && $item->sys->type == 'Link') $result = true;
    elseif (is_array($item) && isset($item[0]->sys) && $item[0]->sys->type == 'Link') $result = true;

    return $result;
  }

  private function getChildren($item, $sublevel = 0)
  {
    $result = [];

    if(is_array($item)) {
      foreach($item as $child) {
        $result[] = $this->getChildFields($child, $sublevel);
      }
    }
    else {
      $result = $this->getChildFields($item, $sublevel);
    }

    if(empty($result)) $result = $item;

    return $result;
  }

  private function getChildFields($item, $sublevel)
  {
    if(!empty($item))
    {
      $linkType = $item->sys->linkType;
      foreach($this->includes->$linkType as $include)
      {
        if($include->sys->id == $item->sys->id && (!isset($include->fields->slug)||$sublevel < 2))
        {
          $item = $include->fields;

          $item->sys = $include->sys;

          if(isset($include->sys->contentType->sys->id))
            $item->sys->contentType = $include->sys->contentType->sys->id;

          foreach($item as $field => $value)
          {
            if($this->isLink($value) && $sublevel < 3)
            {
              $item->$field = $this->getChildren($value, $sublevel+1);
            }
          }
        }
      }
    }
    return $item;
  }


}
