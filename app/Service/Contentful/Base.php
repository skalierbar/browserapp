<?php

namespace Service\Contentful;

class Base
{
  private $space;

  private $token;

  private $locale;

  private $client;

  private $baseUrl = 'https://cdn.contentful.com/spaces/';

  private $baseUri = 'https://cdn.contentful.com/spaces/';

  private $includes;


  public function __construct()
  {
    $f3 = \Base::instance();
    $this->cache = \Cache::instance();
    $this->setContentfulParams($f3);
  }

  private function setContentfulParams($f3)
  {
    $this->space = $f3->get('cf_space_id');

    $this->locale = $f3->get('locale');

    $this->token = $f3->get('cf_access_key');

    $this->previewKey = $f3->get('cf_preview_key');

    $this->preview = $f3->get('cf_preview');

    if ($this->preview) {
      $this->baseUri = 'https://preview.contentful.com/spaces/'.$this->space.'/';
    }
    else {
      $this->baseUri = 'https://cdn.contentful.com/spaces/'.$this->space.'/';
    }

    // $this->client = new \Contentful\Delivery\Client($this->token, $this->space, false, $this->locale, ['cacheDir' => $f3->get('TEMP')]);
  }

  protected function isPreview() { return $this->preview; }

  /**
  * Main Call function to talk to contentful
  */
  protected function call($id, $params = [])
  {
    if(!isset($params['type'])) $type = 'GET'; else $type = $params['type'];
    unset($params['type']);

    $guzzle = new \GuzzleHttp\Client([
      'base_uri' => $this->baseUri
    ]);

    if($this->isPreview()) {
      $params['query']['access_token'] = $this->previewKey;
    } else {
      $params['query']['access_token'] = $this->token;
    }

    $params['query']['locale'] = $this->locale;

    $response = $guzzle->request($type, $id, $params);
    $content = json_decode($response->getBody()->getContents());
    return $content;
  }


  public function getEntries($contentType, $fields = [], $callParams = [])
  {
    $result = [];

    if($this->isContentModelAvailable($contentType))
    {
      $callData = [
        'query' => [
          'content_type' => $contentType,
          'select' => implode(',', $fields)
        ]
      ];

      if(!empty($callParams['skip'])) $callData['query']['skip'] = $callParams['skip'];
      if(!empty($callParams['limit'])) $callData['query']['limit'] = $callParams['limit'];

      $result = $this->call('entries', $callData);
    }

    return $result;
  }

  /**
  * tells if a content model is available in contentful
  **/
  public function isContentModelAvailable($contentType)
  {
    $isAvailable = false;

    $cacheId = $contentType.'-isAvailable';

    if(!$this->cache->exists($cacheId, $isAvailable) || $this->isPreview())
    {
      $items = $this->call('content_types')->items;

      $i = 0;

      do
      {
        $id = $items[$i]->sys->id;
        if($id == $contentType) $isAvailable = true;
        $i++;
      }
      while($i < count($items) && $isAvailable != true);
    }

    return $isAvailable;
  }



  public function getEntriesByContentType($contentType, $fields = [], $params = [])
  {
    $entry = NULL;

    $callData = [
      'query' => [
        'content_type' => $contentType,
        'include' => 3,
      ]
    ];

    if(!is_null($fields)) $callData['query']['select'] = implode(',', $fields);

    foreach($params as $key => $value) $callData['query'][$key] = $value;

    $contents = $this->call('entries', $callData);

    $this->includes = $contents->includes;

    if(!empty($contents))
    {
      foreach($contents->items as $item)
      {
        $beautyItem = $this->fillFields($item->fields);
        $beautyItem->sys = $item->sys;
        $items[] = $beautyItem;
      }
    }

    $items = json_decode(json_encode($items), true);

    return $items;
  }


  /**
  * Returns an entry from contentful in a nice structured array by a given slug
  **/
  public function getEntryBySlug($slug)
  {
    $entry = NULL;

    $contentType = $this->getContentTypeBySlug($slug);

    $id = $contentType.'-'.str_replace('/','-',$slug);

    if(!$this->cache->exists($id, $entry) || $this->isPreview())
    {
      $contents = $this->call('entries',[
        'type' => 'GET',
        'query' => [
          'content_type' => $contentType,
          'fields.slug' => $slug,
          'include' => 4
        ]
      ]);

      $this->includes = $contents->includes;

      if(isset($contents->items[0]->fields))
      {
        $entry = $this->fillFields($contents->items[0]->fields);
        $entry->sys = $contents->items[0]->sys;

        if(isset($entry->sys->contentType)) $entry->sys->contentType = $entry->sys->contentType->sys->id;

        $entry = json_decode(json_encode($entry), true);
      }
    }

    return $entry;
  }

  /**
  * fills fields with linked entry from includes
  */
  private function fillFields($fields, $loop = 0)
  {
    if($loop < 5)
    {

      foreach($fields as $key => $value)
      {
        if($key == "target" || is_string($value)) continue;

        if(isset($value->sys) && $value->sys->type == 'Link') // we have a direct link
        {

          $value = $this->getFieldValue($value);
          $value = $this->fillFields($value, $loop+1);
        }
        elseif(is_array($value) && isset($value[0]->sys))
        {
          foreach($value as $item)
          {
            $item = $this->getFieldValue($item);
            $values[] = $this->fillFields($item, $loop+1);
          }
          $value = $values;
        }
        $fields->$key = $value;
      }
    }

    return $fields;
  }

  /**
  * if link then get that entry from includes
  */
  private function getFieldValue($value)
  {
    if(isset($value->sys))
    {
      $type = $value->sys->linkType;
      $id = $value->sys->id;

      $include = $this->getEntryFromIncludesById($id, $type);

      if(!empty($include)) {

        $value = $include->fields;

        $value->sys = $include->sys;

        if(isset($value->sys->contentType)) $value->sys->contentType = $value->sys->contentType->sys->id;
      }
    }

    return $value;
  }

  /**
  * runs through all includes and finds the right one by id
  */
  private function getEntryFromIncludesById($id, $type)
  {
    $result = NULL;

    if(!empty($this->includes->$type) && is_array($this->includes->$type))
    {
      foreach($this->includes->$type as $item)
      {
        if($item->sys->id == $id) {
          return $item;
        }
      }
    }

    return $result;
  }

  public function getLayoutByTitle($title)
  {
    $entry = NULL;

    $id = 'layout-'.$title;

    if(!$this->cache->exists($id, $entry) || $this->isPreview())
    {
      $contents = $this->call('entries', [
        'query' => [
          'content_type' => 'layout',
          'fields.name' => $title,
          'limit' => 1
        ]
      ]);

      $entry = (new Entry(['id' => $contents->items[0]->sys->id]))->getFields();

      $this->cache->set($id, $entry);

    }
    return $entry;
  }

  /**
  * reads the contentType of a given slug, tries first from cache then from route builder
  **/
  protected function getContentTypeBySlug($slug)
  {
    $contentType = false;

    $slug = str_replace('/','--',$slug);

    if(!$this->cache->exists($slug, $contentType))
    {
      $RouteBuilder = new \Builder\Routes;

      $contentType = $RouteBuilder->getContentTypeOfSlug($slug);

      if(function_exists('bugsnag')) bugsnag()->notifyError('Caching','ContentType not loaded from caching in Contentful Service');
    }

    return $contentType;
  }

  /**
  * Gets the id from the former cached slug
  */
  protected function getIdBySlug($slug)
  {
    if(substr($slug,0,1) != '/') $slug = '/'.$slug;

    if(!empty(\Base::instance()->ROUTES[$slug]))
    {
      $id = end(\Base::instance()->ROUTES[$slug][0]['GET']);
    }

    return $id;
  }

  /**
  * Gets the id from the former cached slug
  */
  public function getConfig($name)
  {
    $config = NULL;

    $id = 'config-'.$name;

    if(!$this->cache->exists($id, $config) || $this->isPreview())
    {
      $response = $this->call('entries', [
        'query' => [
          'content_type' => 'config',
          'fields.name' => $name,
        ]
      ]);

      if(!empty($response->items[0]))
        $config = json_decode(json_encode($response->items[0]->fields), true);

    }
    return $config;
  }

}
