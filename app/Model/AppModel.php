<?php

namespace Model;

abstract class AppModel extends \Contentful\Delivery\Client  {

    protected $f3;
    protected $space;
    protected $token;
    protected $preview;
    protected $cache;
    protected $fields;
    protected $name;

    public function __construct()
    {
      $this->f3 = \Base::instance();

  		$space = $this->f3->get('cf_space_id');
      $token = $this->f3->get('cf_access_key');
  		$preview = $this->f3->get('cf_preview');
  		$locale = $this->f3->get('locale');

      $this->cache = \Cache::instance();

      parent::__construct($token, $space, false, $locale, ['cacheDir' => $this->f3->get('TEMP')]);
    }

    /**
    * Return Content from Contentful Model Route by Slug
    *
    * @param string $slug
    *
    * @return Array
    */
    public function getContentBySlug($slug)
    {
      $contentType = $this->getContentTypeBySlug($slug);
      $cacheVar = $contentType.'-'.str_replace('/','--',$slug);

      if(!$this->cache->exists($cacheVar, $response))
      {
        $cf = new \Service\Contentful;
        $response = $cf->getEntryBySlug($contentType, $slug);

        $this->cache->set($cacheVar, $response);
      }

      return $response;
    }

    private function getContentTypeBySlug($slug)
    {
      $contentType = 'page';

      if(!$this->cache->exists($slug, $contentType)) {
        $RouteBuilder = new \Builders\Routes;

        $contentType = $RouteBuilder->getContentTypeOfSlug($slug);

        if(function_exists('bugsnag')) bugsnag()->notifyError('Caching','ContentType not loaded from caching in AppModel');
      }

      return $contentType;
    }

    protected function getDataArrayFromEntry($entry, $loop = 0)
    {
      $id = $entry->getId();

      if(!$this->cache->exists($id,$result))
      {
        $fields = $this->getFieldsOfContentModel($entry);

        if(isset($fields))
        {
          $result = [
            'id' => $entry->getId(),
            'type' => $entry->getContentType()->getId(),
            'createdAt' => $entry->getCreatedAt()
          ];

          if($loop <= 10)
          {
            foreach ($fields as $field)
            {
              $data = call_user_func(array($entry,'get'.ucfirst($field->id)));

              if(!empty($data))
              {
                switch($field->type)
                {
                  case 'Array':
                    foreach($data as $item)
                    {
                      switch($field->items->linkType)
                      {
                        case 'Asset':
                          $data[] = $this->getDataArrayFromAsset($item);
                          break;

                        case 'Entry':
                          $data[] = $this->getDataArrayFromEntry($item, $loop+1);
                          break;

                        default:
                          $data[] = $item;
                      }
                    }
                    break;

                  case 'Link':

                    switch($field->linkType)
                    {
                      case 'Asset':
                        $data = $this->getDataArrayFromAsset($data);
                        break;

                      case 'Entry':
                        $data = $this->getDataArrayFromEntry($data, $loop+1);
                        break;
                    }

                    break;

                  case 'Asset':
                    $data = $this->getDataArrayFromAsset($data, $loop+1);
                    break;

                }
              }

              if(is_array($data)) $data = "verdammt";

              $result[$field->id] = $data;
            }
          }
        }

        // debug($result);

        $this->cache->set($id, $result);

      }
      return $result;
    }

    /**
    * Reads content model structure from cached contentful filesystem
    *
    * @return object
    */
    private function getFieldsOfContentModel($entry)
    {
      $fields = NULL;

      if($entry->getContentType() !== NULL)
      {
        $contentType = $entry->getContentType()->getId();
        $spaceId = $this->getSpace()->getId();
        $fsc = new \Contentful\Delivery\Cache\FilesystemCache($this->f3->get('TEMP'), $spaceId);
        $result = $fsc->readContentType($contentType);

        if(!isset($result)) {
          $this->initCache();
          $fields = $this->getFieldsOfContentModel($entry);
        }
        else $fields = json_decode($result)->fields;
      }
      return $fields;
    }

    /**
     * Builds the array for an asset with title, description and URL
     *
     * @param object $asset
     *
     * @return array
     */
    private function getDataArrayFromAsset($asset)
    {
      $result = array(
        'title' => $asset->getTitle(),
        'description' => $asset->getDescription(),
        'url' => $asset->getFile()->getUrl(),
      );
      return $result;
    }

    /**
     * Runs the Contentful Cache WarmUp in the F3 Temp folder
     *
     */
    private function initCache()
    {
      $cacheWarmer = new \Contentful\Delivery\Cache\CacheWarmer($this);
      $cacheWarmer->warmUp('../tmp');
    }

}
