<?php

namespace Model;

class BlogPost extends AppModel
{

  // public function __construct()
  // {
  //   parent::__construct();
  // }
  //
  // private function getPostSlug($slug)
  // {
	// 	$uri = explode('/', $slug);
  //   $num = count($uri);
	// 	if($num > 0) {
	// 		$slug = $uri[$num-1];
	// 	}
	// 	return $slug;
	// }

  // public function getPostBySlug($slug)
  // {
  //   $cacheVar = 'blogPost-'.str_replace('/','--',$slug);
  //
  //   if(!$this->cache->exists($cacheVar, $response))
  //   {
  //     $cf = new \Service\Contentful;
  //     $response = $cf->getEntryBySlug('blogPost', $slug);
  //
  //     $this->cache->set($cacheVar, $response);
  //   }
  //
  //   return $response;
  // }

  public function getRecentPosts($contentType = "blogPost", $orderBy = "")
  {
    $cacheVar = $contentType.'-'.str_replace('/','--',$orderBy);

    if(!$this->cache->exists($cacheVar, $response))
    {
      $cf = new \Service\Contentful\Base;
      $response = $cf->getEntriesByContentType($contentType, [], ['order' => $orderBy]);

      $this->cache->set($cacheVar, $response);
    }

    return $response;
  }

}
