<?php

namespace Model;

class Content extends AppModel
{

  public function __construct($contentTypeId)
  {
    $this->name = $contentTypeId;

    parent::__construct();
  }
}
