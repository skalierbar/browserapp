<?php

namespace Model;

class BlogCategory extends AppModel
{

  public function __construct()
  {
    parent::__construct();
  }

  private function getSlug($slug)
  {
		$uri = explode('/', $slug);
    $num = count($uri);
		if($num > 0) {
			$slug = $uri[$num-1];
		}
		return $slug;
	}

  public function getCategoryBySlug($slug)
  {
    $slug = $this->getSlug($slug);
    $content = $this->getContentBySlug($slug, 'blogCategory');
    return $content;
  }

}
